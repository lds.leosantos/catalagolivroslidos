<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\RedesSociaisTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\RedesSociaisTable Test Case
 */
class RedesSociaisTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\RedesSociaisTable
     */
    protected $RedesSociais;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'app.RedesSociais',
        'app.IconesRedesSociais',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('RedesSociais') ? [] : ['className' => RedesSociaisTable::class];
        $this->RedesSociais = TableRegistry::getTableLocator()->get('RedesSociais', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->RedesSociais);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
