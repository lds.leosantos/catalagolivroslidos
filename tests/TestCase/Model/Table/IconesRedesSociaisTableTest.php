<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\IconesRedesSociaisTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\IconesRedesSociaisTable Test Case
 */
class IconesRedesSociaisTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\IconesRedesSociaisTable
     */
    protected $IconesRedesSociais;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'app.IconesRedesSociais',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('IconesRedesSociais') ? [] : ['className' => IconesRedesSociaisTable::class];
        $this->IconesRedesSociais = TableRegistry::getTableLocator()->get('IconesRedesSociais', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->IconesRedesSociais);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
