<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ResenhasTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ResenhasTable Test Case
 */
class ResenhasTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\ResenhasTable
     */
    protected $Resenhas;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'app.Resenhas',
        'app.Livros',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('Resenhas') ? [] : ['className' => ResenhasTable::class];
        $this->Resenhas = TableRegistry::getTableLocator()->get('Resenhas', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->Resenhas);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
