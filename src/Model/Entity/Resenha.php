<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Resenha Entity
 *
 * @property int $id
 * @property string $titulo
 * @property string $descricao
 * @property int $livro_id
 * @property int $ativo
 * @property int $qtn_acesso
 * @property string|null $comentarios
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime|null $modified
 *
 * @property \App\Model\Entity\Livro $livro
 */
class Resenha extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'titulo' => true,
        'descricao' => true,
        'livro_id' => true,
        'ativo' => true,
        'qtn_acesso' => true,
        'curtidas' => true,
        'created' => true,
        'modified' => true,
        'livro' => true,
    ];
}
