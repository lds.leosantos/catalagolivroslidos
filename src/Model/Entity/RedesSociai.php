<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * RedesSociai Entity
 *
 * @property int $id
 * @property string $link
 * @property int $ativo
 * @property int $icones_redes_sociais_id
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime|null $modified
 *
 * @property \App\Model\Entity\IconesRedesSociai $icones_redes_sociai
 */
class RedesSociai extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'link' => true,
        'ativo' => true,
        'icones_redes_sociais_id' => true,
        'created' => true,
        'modified' => true,
        'icones_redes_sociai' => true,
    ];
}
