<?php

declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Resenhas Model
 *
 * @property \App\Model\Table\LivrosTable&\Cake\ORM\Association\BelongsTo $Livros
 *
 * @method \App\Model\Entity\Resenha newEmptyEntity()
 * @method \App\Model\Entity\Resenha newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\Resenha[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Resenha get($primaryKey, $options = [])
 * @method \App\Model\Entity\Resenha findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\Resenha patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Resenha[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\Resenha|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Resenha saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Resenha[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Resenha[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\Resenha[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Resenha[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class ResenhasTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('resenhas');
        $this->setDisplayField('titulo');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Livros', [
            'foreignKey' => 'livro_id',
            'joinType' => 'INNER',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('titulo')
            ->maxLength('titulo', 220)
            ->notEmptyString('titulo');

        // $validator
        //     ->scalar('descricao')
        //     ->notEmptyString('descricao');

        $validator->boolean('ativo')->requirePresence('ativo', 'create')->notEmptyString('ativo');

        $validator
            ->integer('qtn_acesso')
            ->notEmptyString('qtn_acesso');

            $validator
            ->integer('curtidas')
            ->notEmptyString('curtidas');

      

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn(['livro_id'], 'Livros'));

        return $rules;
    }


    public function getListarResenhasHome()
    {
        $query = $this->find()
            ->select(['id', 'titulo', 'descricao', 'qtn_acesso', 'curtidas', 'livro_id'])
            ->contain(['Livros'])
            ->where([
                'Resenhas.ativo = ' => 1,
            ])
            ->limit(4)
            ->order(['Resenhas.id' => 'DESC']);
        return $query;
    }


    public function getVerResenha($id)
    {

        $query = $this->find()
            ->select(['id', 'titulo', 'descricao','livro_id', 'curtidas','created'])
            ->contain(['Livros'])
            ->where(['Resenhas.id' => $id])
            ->first();
        return $query;
    }

    public function getResenhasDestaques()
    {
        $query = $this->find()
            ->select(['id','titulo', 'descricao','livro_id'])
            ->where([
                'Resenhas.ativo =' => 1
            ])
            ->order(['Resenhas.qtn_acesso' => 'DESC'])
            ->limit(3);
        return $query;
    }


    public function getListarUltimoResenhas()
    {
        $query = $this->find()
            ->select(['id','titulo', 'descricao'])
            ->where([
                'Resenhas.ativo =' => 1
            ])
            ->order(['Resenhas.id' => 'DESC'])
            ->limit(3);
        return $query;
    }
}
