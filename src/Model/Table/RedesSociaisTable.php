<?php

declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * RedesSociais Model
 *
 * @property \App\Model\Table\IconesRedesSociaisTable&\Cake\ORM\Association\BelongsTo $IconesRedesSociais
 *
 * @method \App\Model\Entity\RedesSociai newEmptyEntity()
 * @method \App\Model\Entity\RedesSociai newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\RedesSociai[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\RedesSociai get($primaryKey, $options = [])
 * @method \App\Model\Entity\RedesSociai findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\RedesSociai patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\RedesSociai[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\RedesSociai|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\RedesSociai saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\RedesSociai[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\RedesSociai[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\RedesSociai[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\RedesSociai[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class RedesSociaisTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('redes_sociais');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('IconesRedesSociais', [
            'foreignKey' => 'icones_redes_sociais_id',
            'joinType' => 'INNER',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('link')
            ->maxLength('link', 200)
            // ->requirePresence('link', 'create')
            ->notEmptyString('link');

        $validator
            ->notEmptyString('ativo');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn(['icones_redes_sociais_id'], 'IconesRedesSociais'));

        return $rules;
    }

  
}
