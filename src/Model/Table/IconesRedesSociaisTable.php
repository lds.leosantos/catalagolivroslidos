<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * IconesRedesSociais Model
 *
 * @method \App\Model\Entity\IconesRedesSociai newEmptyEntity()
 * @method \App\Model\Entity\IconesRedesSociai newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\IconesRedesSociai[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\IconesRedesSociai get($primaryKey, $options = [])
 * @method \App\Model\Entity\IconesRedesSociai findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\IconesRedesSociai patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\IconesRedesSociai[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\IconesRedesSociai|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\IconesRedesSociai saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\IconesRedesSociai[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\IconesRedesSociai[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\IconesRedesSociai[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\IconesRedesSociai[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class IconesRedesSociaisTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('icones_redes_sociais');
        $this->setDisplayField('titulo');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('titulo')
            ->maxLength('titulo', 50)
            ->requirePresence('titulo', 'create')
            ->notEmptyString('titulo');

        $validator
            ->scalar('icone')
            ->maxLength('icone', 50)
            ->requirePresence('icone', 'create')
            ->notEmptyString('icone');

        return $validator;
    }
}
