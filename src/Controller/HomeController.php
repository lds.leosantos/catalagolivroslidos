<?php

declare(strict_types=1);

namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\EventInterface;
use Laminas\Diactoros\UploadedFile;

use Cake\Database\Exception\QueryExpression;
use Cake\Database\Expression\QueryExpression as ExpressionQueryExpression;
use Cake\ORM\TableRegistry;

class HomeController extends AppController
{

    public function beforeFilter(EventInterface $event)
    {
        $this->Auth->allow(['index', 'view', 'curtidas', 'comentarios']);
    }

    public function index()
    {
        $resenhasTable = $this->getTableLocator()->get('Resenhas');
        $resenhasAtivas = $resenhasTable->getListarResenhasHome();

        $ultimasResenhas = $resenhasTable->getListarUltimoResenhas();
        $resenhasDestaques = $resenhasTable->getResenhasDestaques();

       

        $this->set(compact('resenhasAtivas', 'ultimasResenhas', 'resenhasDestaques'));
    }

    public function view($id = null)
    {
        $resenhasTable = $this->getTableLocator()->get('Resenhas');
        $resenha = $resenhasTable->getVerResenha($id);

        if ($resenha) {
            $vizualicaoes = new ExpressionQueryExpression('qtn_acesso = qtn_acesso + 1');
            $resenhasTable->updateAll([$vizualicaoes], ['Resenhas.id' => $resenha->id]);
        }

        $this->set(compact('resenha'));
    }

    public function curtidas($id = null)
    {
        $resenhasTable = $this->getTableLocator()->get('Resenhas');
        $resenha = $resenhasTable->getVerResenha($id);

        if ($resenha) {
            $curtidas = new ExpressionQueryExpression('curtidas = curtidas + 1');
            $resenhasTable->updateAll([$curtidas], ['Resenhas.id' => $resenha->id]);
            return $this->redirect(['action' => 'view', $resenha->id]);
        }
    }

    public function comentarios($id = null)
    {
        $comentariotable = $this->getTableLocator()->get('Comentarios');
        $contatoMsg = $comentariotable->newEmptyEntity();
        $resenha_id  = $id;
        if ($this->request->is(['patch', 'post', 'put'])) {
            $contatoMsg = $comentariotable->patchEntity($contatoMsg, $this->request->getData());
            $contatoMsg->resenha_id = $resenha_id;
            $contatoMsg->resenha_id;

            if ($comentariotable->save($contatoMsg)) {
                $this->Flash->success(__('Comentário enviado com sucesso!'));
                return $this->redirect(['action' => 'view', $contatoMsg->resenha_id]);
            } else {
                $this->Flash->danger(__('Error: Conmentário  não foi enviado com sucesso.
                     Preencha Todos os campos!.'));
            }
        }

        $this->set(compact('contatoMsg'));
    }
}
