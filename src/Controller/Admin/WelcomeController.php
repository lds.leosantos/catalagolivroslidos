<?php

declare(strict_types=1);

namespace App\Controller\Admin;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use Cake\Datasource\ConnectionManager;


class WelcomeController extends AppController
{

    public  function  index()
    {

        $con = ConnectionManager::get('default');
        $result = $con->prepare(' SELECT (SUM(numero_paginas)) AS soma FROM livros Livros');
        $result->execute();
        $total_paginas = $result->fetch('assoc');
        implode($total_paginas);

        $cont_curtidas =   $con->prepare(' SELECT (SUM(curtidas)) AS soma FROM resenhas Resenhas');
        $cont_curtidas->execute();
        $total_curtidas = $cont_curtidas->fetch('assoc');
        implode($total_curtidas);


        $cont_view = $con->prepare(' SELECT (SUM(qtn_acesso)) AS soma FROM resenhas Resenhas');
        $cont_view->execute();
        $total_views = $cont_view->fetch('assoc');
        implode($total_views);

        $total_livros  = $this->getTableLocator()->get('Livros')->find('all')->count();
        $resenhas  = $this->getTableLocator()->get('Resenhas')->find('all')->count();
        $comentarios  = $this->getTableLocator()->get('Comentarios')->find('all')->count();

        $total_livros  = $this->getTableLocator()->get('Livros')->find('all')->count();

        // $dadosuser  = TableRegistry::getTableLocator()->get('Users')->find()->select(['name','username'])->first();

        $user_id = $this->Auth->user('id');
        $userTable =   $this->getTableLocator()->get('Users');
        $dadosuser =    $userTable->getUserDados($user_id);


        $this->set(compact('total_livros', 'total_paginas', 'dadosuser','resenhas','comentarios','total_curtidas','total_views'));
    }
}
