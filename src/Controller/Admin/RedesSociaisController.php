<?php

declare(strict_types=1);

namespace App\Controller\Admin;

use App\Controller\AppController;

/**
 * RedesSociais Controller
 *
 * @property \App\Model\Table\RedesSociaisTable $RedesSociais
 * @method \App\Model\Entity\RedesSociai[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class RedesSociaisController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['IconesRedesSociais'],
        ];
        $redesSociais = $this->paginate($this->RedesSociais);

        $this->set(compact('redesSociais'));
    }

    /**
     * View method
     *
     * @param string|null $id Redes Sociai id.
     * @return \Cake\Http\Response|null|void Renders view
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $redesSociai = $this->RedesSociais->get($id, [
            'contain' => ['IconesRedesSociais'],
        ]);

        $this->set(compact('redesSociai'));
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $redesSociai = $this->RedesSociais->newEmptyEntity();
        if ($this->request->is('post')) {
            $redesSociai = $this->RedesSociais->patchEntity($redesSociai, $this->request->getData());
            if ($this->RedesSociais->save($redesSociai)) {
                $this->Flash->success(__('Rede Social salva com sucesso.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('Erro: A Rede Social não foi salva com sucesso. Por favor, tente novamente'));
        }
        $iconesRedesSociais = $this->RedesSociais->IconesRedesSociais->find('list', ['limit' => 200]);
        $this->set(compact('redesSociai', 'iconesRedesSociais'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Redes Sociai id.
     * @return \Cake\Http\Response|null|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $redesSociai = $this->RedesSociais->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $redesSociai = $this->RedesSociais->patchEntity($redesSociai, $this->request->getData());
            if ($this->RedesSociais->save($redesSociai)) {
                $this->Flash->success(__('Rede Social editada com sucesso.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('Erro: A Rede Social não foi editada com sucesso. Por favor, tente novamente'));
        }
        $iconesRedesSociais = $this->RedesSociais->IconesRedesSociais->find('list', ['limit' => 200]);
        $this->set(compact('redesSociai', 'iconesRedesSociais'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Redes Sociai id.
     * @return \Cake\Http\Response|null|void Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $redesSociai = $this->RedesSociais->get($id);
        if ($this->RedesSociais->delete($redesSociai)) {
            $this->Flash->success(__('Rede Social excluida com sucesso.'));
        } else {
            $this->Flash->error(__('Erro: A Rede Social não foi excluida com sucesso. Por favor, tente novamente'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
