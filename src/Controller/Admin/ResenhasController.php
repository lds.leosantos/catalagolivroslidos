<?php
declare(strict_types=1);

namespace App\Controller\Admin;
use App\Controller\AppController;

/**
 * Resenhas Controller
 *
 * @property \App\Model\Table\ResenhasTable $Resenhas
 * @method \App\Model\Entity\Resenha[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ResenhasController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Livros'],
        ];
        $resenhas = $this->paginate($this->Resenhas);

        $this->set(compact('resenhas'));
    }

    /**
     * View method
     *
     * @param string|null $id Resenha id.
     * @return \Cake\Http\Response|null|void Renders view
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $resenha = $this->Resenhas->get($id, [
            'contain' => ['Livros'],
        ]);

        $this->set(compact('resenha'));
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $resenha = $this->Resenhas->newEmptyEntity();
        
        if ($this->request->is('post')) {
            $resenha = $this->Resenhas->patchEntity($resenha, $this->request->getData());
           
            if ($this->Resenhas->save($resenha)) {
                $this->Flash->success(__('A resenha foi salva com sucesso!.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('A resenha não foi salva. Por favor, tente novamente.'));
        }
        $livros = $this->Resenhas->Livros->find('list', ['limit' => 200]);
        $this->set(compact('resenha', 'livros'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Resenha id.
     * @return \Cake\Http\Response|null|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $resenha = $this->Resenhas->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $resenha = $this->Resenhas->patchEntity($resenha, $this->request->getData());
            if ($this->Resenhas->save($resenha)) {
                $this->Flash->success(__('A resenha foi editada com sucesso!.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('A resenha não foi editada. Por favor, tente novamente.'));
        }
        $livros = $this->Resenhas->Livros->find('list', ['limit' => 200]);
        $this->set(compact('resenha', 'livros'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Resenha id.
     * @return \Cake\Http\Response|null|void Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $resenha = $this->Resenhas->get($id);
        if ($this->Resenhas->delete($resenha)) {
            $this->Flash->success(__('A resenha foi deletada com sucesso!.'));
        } else {
            $this->Flash->error(__('A resenha não foi deletada. Por favor, tente novamente.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
