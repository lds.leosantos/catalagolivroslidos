<?php

declare(strict_types=1);

namespace App\View\Helper;

use Cake\ORM\TableRegistry;
use Cake\View\Helper;
use Cake\View\View;

/**
 * Search helper
 */
class SearchHelper extends Helper
{
    /**
     * Default configuration.
     *
     * @var array
     */
    protected $_defaultConfig = [];





    public function getLivros($id)
    {

        $query = TableRegistry::getTableLocator()->get('Livros')
            ->find()
            ->select(['nome_livro', 'nome_autor', 'serie', 'volume', 'data_inicio_leitura', 'data_fim_leitura'])
            ->where(['Livros.id' => $id])
            ->first();
        return $query;
    }

    public function getFooter($id)
    {

        $query = TableRegistry::getTableLocator()->get('Footers')
            ->find()
            ->select(['descricao', 'autor_frase'])
            ->where(['Footers.id' => $id])
            ->first();
        return $query;
    }

    public function getlistarLivro($id)
    {
        $query = TableRegistry::getTableLocator()->get('Livros')
            ->find()
            ->select(['id', 'nome_livro', 'imagem'])
            ->where(['Livros.id =' => $id])
            ->first();
        return $query;
    }

    public function getlistarComentarios($id)
    {
        $query = TableRegistry::getTableLocator()->get('Comentarios')
            ->find()
            ->select(['id', 'nome_autor', 'mensagem', 'resenha_id', 'created'])
            ->where(['Comentarios.resenha_id =' => $id]);
        return $query;
    }

    public function getlistarTotalComentarios($id)
    {
        $query = TableRegistry::getTableLocator()->get('Comentarios')->find('all')
            ->where(['Comentarios.resenha_id =' => $id])
            ->count();
        return $query;
    }

    public function getListarResenha($id)
    {
        $query = TableRegistry::getTableLocator()->get('Resenhas')
            ->find()
            ->select(['id', 'livro_id'])
            ->where(['Resenhas.id =' => $id])
            ->first();
        return $query;
    }

    public function getlistarRedesSociaisIcones($id)
    {
        $query = TableRegistry::getTableLocator()->get('IconesRedesSociais')
            ->find()
            ->select(['id', 'titulo', 'icone'])
            ->where(['IconesRedesSociais.id =' => $id])
            ->first();
        return $query;
    }

    public function getListRedesSociais()

    {
        $query = TableRegistry::getTableLocator()->get('RedesSociais')
            ->find()
            ->select(['id', 'link', 'icones_redes_sociais_id'])
            ->where(['RedesSociais.ativo =' => 1])
            ->order(['RedesSociais.id' => 'ASC']);
        return $query;
    }
}
