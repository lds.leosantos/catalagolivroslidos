<?php $cakeDescription = 'Blog'; ?>
<!DOCTYPE html>
<html lang="pt-BR">

<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>
        <?= $cakeDescription ?>
    </title>
    <?= $this->Html->meta('icon') ?>
    <!-- Font -->

    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500" rel="stylesheet">
    <?= $this->Html->css([
        'bootstrap.min', 'fontawesome-all', 'blog-sidebar/css/styles',
        'blog-sidebar/css/responsive'
    ]) ?>
    <?= $this->Html->script([
        'jquery-3.5.1.min', 'popper.min', 'common-js/tether.min.js', 'bootstrap.min', 'fontawesome-all', 'common-js/scripts.js'
    ]) ?>

    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>
</head>

<body>
    <?= $this->element('header_blog') ?>
    <?= $this->element('content_blog') ?>


    <?= $this->element('footer_blog') ?>





</body>

</html>