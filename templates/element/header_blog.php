<header>

    <div class="container-fluid position-relative no-side-padding">
        <?php $imagem = $this->Html->image('../img/logo.png', ['class' => 'logo', 'alt' => 'Logo']);
        echo $this->Html->link(__($imagem), ['Controller' => 'Home', 'action' => 'index'], ['escape' => false]);
        ?>
        <!-- <a href="#" class="logo"><img src="images/logo.png" alt="Logo Image"></a>  -->


        <div class="menu-nav-icon" data-nav-menu="#main-menu"><i class="ion-navicon"></i></div>

        <ul class="main-menu visible-on-click" id="main-menu">
            <li><?= $this->Html->link(__('Home'),['controller'=>'Home','action'=>'index']) ?></li>
        </ul><!-- main-menu -->

     

    </div><!-- conatiner -->
</header>