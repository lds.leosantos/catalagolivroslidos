<?php
$redeSociais = $this->Search->getListRedesSociais();
?>
<footer>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="footer-section">
                    <?php $imagem = $this->Html->image('../img/logo.png', ['class' => 'logo', 'alt' => 'Logo']);
                    echo $this->Html->link(__($imagem), ['Controller' => 'Home', 'action' => 'index'], ['escape' => false]);
                    ?>
                    <p class="copyright">&copy; 2019 - <?php echo date('Y'); ?> Desenvolvido por HLTI - PHP DEVELOPERS. Todos os Direiros Reservados.</p>
                    <ul class="icons">
                        <?php foreach ($redeSociais as $redesocial) { ?>
                            <?php
                                $redes = $this->Search->getlistarRedesSociaisIcones($redesocial->icones_redes_sociais_id);
                            ?>
                            <li>
                                <a href="<?= $redesocial->link ?>" target="_blank">
                                    <i class="<?= $redes->icone ?>"></i>
                                    <?= $redes->titulo ?>
                                </a>
                            </li>
                        <?php } ?>
                    </ul>
                </div><!-- footer-section -->
            </div><!-- col-md-12 -->
        </div><!-- row -->
    </div><!-- container -->
</footer>