<?php

/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Comentario $comentario
 */
?>


<?php
$resenha = $this->Search->getListarResenha($comentario->resenha->id);
$livro = $this->Search->getlistarLivro($resenha->livro_id);
?>
<div class="container-fluid">
    <div class="col-md-12">
        <div class="table-responsive rounded mt-5">
            <h3>
                <?= $this->Html->link(
                    '<i class="fas fa-undo"></i> Retornar',
                    ['controller' => 'Comentarios', 'action' => 'index'],
                    ['class' => 'btn btn-info btn-lg float-right mb-5', 'escape' => false]
                ) ?>
            </h3>
        </div>
        <div class="container-fluid card mb-5 ">
            <div class="card-body">
                <h6 class="alert alert-success text-center">DADOS DO COMENTÁRIO </h6>
                <table class="table table-hover table-bordered rounded">
                    <tr>
                        <td>
                            <b>NOME DO AUTOR: <?= h($comentario->nome_autor) ?></b>
                        </td>
                        <td>
                            <b>TITULO DA RESENHA: <?= h($comentario->resenha->titulo) ?></b>
                        </td>

                    </tr>
                    <tr>
                        <td>
                            <b>LIVRO DA RESENHA: <?= h($livro->nome_livro) ?></b>
                        </td>
                        <td>
                            <b>DATA CADASTRO: <?= h($comentario->created) ?></b>
                        </td>
                    </tr>
                </table>
                <h6 class="alert alert-success text-center">COMENTÁRIO </h6>
                <table class="table table-hover table-bordered rounded">
                    <div class="container-fluid card mb-3">
                        <div class=" card-body mb-5"> <?php echo $comentario->mensagem;   ?>
                        </div>
                    </div>
                </table>
            </div>
        </div>
    </div>
</div>