<?php

/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Comentario[]|\Cake\Collection\CollectionInterface $comentarios
 */
?>

<?php

/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Resenha[]|\Cake\Collection\CollectionInterface $resenhas
 */
?>


<?= $this->Html->css(['dataTables.bootstrap.min']) ?>
<?= $this->Html->script(['jquery.dataTables.min', 'dataTables.bootstrap4.min', 'dataTables.buttons.min', 'buttons.html5.min']) ?>

<div class="col-md-12">
    <div class="table-responsive mt-5">

        <h3 class="text-center">
            COMENTÁRIOS
        </h3>
    </div>
    <br>
    <div class="card">

        <table id="datatable" class="table table-striped table-bordered table-hover text-center rounded">

            <thead>
                <tr>
                    <th>ID</th>

                    <th>TITULO DA RESENHA</th>
                    <th>NOME DO AUTOR DO COMENTÁRIO</th>
                    <th>DATA CADASTRO</th>
                    <th class="text-center">AÇÕES</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($comentarios as $comentario) : ?>
                    <tr>
                        <td class="align-middle"><?= h($comentario->id) ?></td>
                        <td class="align-middle"><?= h($comentario->resenha->titulo) ?></td>
                        <td class="align-middle"><?= h($comentario->nome_autor) ?></td>
                        <td class="align-middle"><?= h($comentario->created) ?></td>
                        <td class="align-middle">
                            <?= $this->Html->link(('<i class="far fa-eye"></i>'), ['controller' => 'Comentarios', 'action' => 'view', $comentario->id], ['class' => 'btn btn-outline-primary', 'escape' => false]) ?>
                            <?= $this->Html->link('<i class="far fa-edit "></i>', ['controller' => 'Comentarios', 'action' =>  'edit', $comentario->id], ['class' =>  'btn btn-outline-warning',  'escape' => false]) ?>
                            <?= $this->Form->postLink('<i class="fas fa-trash"></i>', ['controller' => 'Comentarios', 'action' => 'delete', $comentario->id], ['class' => 'btn btn-outline-danger', 'escape' => false, 'confirm' => __('Realmente deseja apagar o Comentário  # {0}?', $comentario->id)]) ?>
                        </td>

                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>



