<?php

/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Comentario $comentario
 */
?>
<div class="d-flex">
    <div class="mr-auto p-2">
        <h2 class="display-4 titulo">Editar Comentário</h2>
    </div>
    <div class="p-2">
        <span class="d-none d-md-block">
            <?= $this->Html->link(
                __('Listar'),
                ['controller' => 'Comentarios', 'action' => 'index'],
                ['class' => 'btn btn-outline-info btn-sm']
            ) ?>
        </span>
        <div class="dropdown d-block d-md-none">
            <button class="btn btn-primary dropdown-toggle btn-sm" type="button" id="acoesListar" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Ações
            </button>
            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="acoesListar">
                <?= $this->Html->link(__('Listar'), ['controller' => 'Comentarios', 'action' => 'index'], ['class' => 'dropdown-item']) ?>
            </div>
        </div>
    </div>
</div>
<hr>

<?= $this->Flash->render() ?>
<?= $this->Form->create($comentario) ?>
<div class="form-row">
    <div class="form-group col-md-12">
        <label><span class="text-danger">*</span> Resenha</label>
        <?= $this->Form->control('resenha_id', ['options' => $resenhas, 'class' => 'form-control', 'placeholder' => 'Titulo da Resenha', 'label' => false, 'empty' => true]) ?>
    </div>
</div>
<div class="form-row">
    <div class="form-group col-md-12">
        <label><span class="text-danger">*</span> Nome</label>
        <?= $this->Form->control('nome_autor', ['class' => 'form-control', 'placeholder' => 'Nome', 'label' => false]) ?>
    </div>
</div>
<div class="form-row">
    <div class="form-group col-md-12">
        <label><span class="text-danger">*</span> Mensagem</label>
        <?= $this->Form->control('mensagem', ['type' => 'textarea', 'class' => 'form-control', 'placeholder' => 'Mensagem', 'label' => false]) ?>
    </div>
</div>

<p>
    <span class="text-danger">* </span>Campo obrigatório
</p>
<?= $this->Form->button(__('Editar'), ['class' => 'btn btn-warning'])  ?>
<?= $this->Form->end() ?>

