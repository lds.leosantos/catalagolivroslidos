<?php

/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\RedesSociai $redesSociai
 */
?>
<?php

/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Resenha $resenha
 */
?>
<div class="container-fluid">
    <div class="col-md-12">
        <div class="table-responsive rounded mt-5">
            <h3>
                <?= $this->Html->link(
                    '<i class="fas fa-undo"></i> Retornar',
                    ['controller' => 'RedesSociais', 'action' => 'index'],
                    ['class' => 'btn btn-info btn-lg float-right mb-5', 'escape' => false]
                ) ?>
            </h3>
        </div>

        <div class="container-fluid card mb-5 ">
            <div class="card-body">
                <h6 class="alert alert-success text-center">DADOS DA REDE SOCIAL</h6>
                <table class="table table-hover table-bordered rounded">
                    <tr>
                        <td>
                            <b>NOME DA REDE SOCIAL: <?= h($redesSociai->icones_redes_sociai->titulo) ?></b>
                        </td>
                        <td>
                            <b>LINK: <?= h($redesSociai->link) ?></b>
                        </td>
                        <td>
                            <b>ATIVA: <?= h($redesSociai->ativo) ? __('SIM') : __('NÃO') ?></b>
                        </td>

                    </tr>
                    <tr>

                        <td>
                            <b> DATA DA CRIAÇÂO: <?= h($redesSociai->created) ?></b>
                        </td>
                        <td>
                            <b> DATA DA MODIFICAÇÂO: <?= h($redesSociai->modified) ?></b>

                        </td>
                        <td></td>
                    </tr>
                </table>


            </div>
        </div>
    </div>
</div>

