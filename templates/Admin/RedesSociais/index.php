<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\RedesSociai[]|\Cake\Collection\CollectionInterface $redesSociais
 */
?>



<?= $this->Html->css(['dataTables.bootstrap.min']) ?>
<?= $this->Html->script(['jquery.dataTables.min', 'dataTables.bootstrap4.min', 'dataTables.buttons.min', 'buttons.html5.min']) ?>

<div class="col-md-12">
    <div class="table-responsive mt-5">
        <h3>
            <?= $this->Html->link('<i class="fas fa-plus fa-2x"></i>', ['controller' => 'RedesSociais', 'action' => 'add'], ['class' => 'btn btn-primary btn-lg rounded-circle float-right', 'escape' => false]) ?>
        </h3>
        <h3 class="text-center">
            REDES SOCIAIS
        </h3>
    </div>
    <br>
    <div class="card">

        <table id="datatable" class="table table-striped table-bordered table-hover text-center rounded">

            <thead>
                <tr>

                    <th>NOME DA REDE SOCIAL</th>
                    <th>LINK</th>
                    <th>ATIVO</th>
                    <th>DATA CADASTRO</th>
                    <th class="text-center">AÇÕES</th>
                </tr>
            </thead>
            <tbody>
            <?php foreach ($redesSociais as $redesSociai): ?>
                    <tr>

                        <td class="align-middle"><?= h($redesSociai->icones_redes_sociai->titulo) ?></td>
                        <td class="align-middle"><?= h($redesSociai->link) ?></td>
                        <td class="align-middle"><?= h($redesSociai->ativo) ? __('SIM') : __('NÃO') ?></td>
                        <td class="align-middle"><?= h($redesSociai->created) ?></td>
                        <td class="align-middle">
                            <?= $this->Html->link(('<i class="far fa-eye"></i>'), ['controller' => 'RedesSociais', 'action' => 'view', $redesSociai->id], ['class' => 'btn btn-outline-primary', 'escape' => false]) ?>
                            <?= $this->Html->link('<i class="far fa-edit "></i>', ['controller' => 'RedesSociais', 'action' =>  'edit', $redesSociai->id], ['class' =>  'btn btn-outline-warning',  'escape' => false]) ?>
                            <?= $this->Form->postLink('<i class="fas fa-trash"></i>', ['controller' => 'RedesSociais', 'action' => 'delete', $redesSociai->id], ['class' => 'btn btn-outline-danger', 'escape' => false, 'confirm' => __('Realmente deseja apagar a Rede Social  # {0}?', $redesSociai->icones_redes_sociai->titulo)]) ?>
                        </td>

                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>
