<?php

/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\RedesSociai $redesSociai
 */
?>

<?= $this->Html->css(['fileinput.min', 'nprogress', 'component-chosen', 'switchery']) ?>
<div class="container-fluid mt-5">
    <?= $this->Form->create($redesSociai) ?>
    <fieldset>
        <legend class="text-center mb-5">
            <h3> ADICIONAR NOVA REDE SOCIAL </h3>
        </legend>
        <div class="card">
            <div class="card-body">
                <div class="form-row">
                    <div class="form-group col-md-12">
                        <strong><label>NOME DA REDE SOCIAL</label></strong>
                        <?= $this->Form->control('icones_redes_sociais_id', ['options' => $iconesRedesSociais, 'empty' => 'Selecione a Rede Social', 'class' => 'form-contro form-control-chosen',  'required' => true, 'placeholder' => 'Nome da Rede Social', 'label' => false]) ?>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-12">
                        <strong><label>LINK</label></strong>
                        <?= $this->Form->control('link', ['class' => 'form-control', 'placeholder'=>'https://www.google.com', 'required' => true, 'label' => false]) ?>
                    </div>
                </div>
                <div class="form-row">
                    <div class="col-md-4 ml-0 mt-2 row block">
                        <label class="">REDE ATIVA<A/label> <div class="px-4 mb-">
                                <?= $this->Form->control('ativo', ['class' => 'ml-5 js-switch', 'type' => 'checkbox', 'label' => false]) ?>
                    </div>
                </div>
            </div>
        </div>
</div>

<div class=" justify-content-center text-center mb-5">
    <?= $this->Form->button('<i class="fas fa-save"></i> SALVAR', ['class' => 'btn btn-primary mt-5', 'escapeTitle' => false]) ?>
    <?= $this->Html->link('CANCELAR', ['controller' => 'RedesSociais', 'action' => 'index'], ['class' => 'btn btn-danger mt-5', 'escape' => false]) ?>
    <?= $this->Form->end() ?>
</div>
</fieldset>
</div>

<?= $this->Html->script(['switchery', 'chosen.jquery.min', 'nprogress', 'jquery.maskedinput']) ?>
<?= $this->element('editor'); ?>

<script>
    $(".form-control-chosen").chosen();
</script>