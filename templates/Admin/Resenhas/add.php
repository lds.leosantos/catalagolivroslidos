<?= $this->Html->css(['fileinput.min', 'nprogress', 'component-chosen','switchery']) ?>
<div class="container-fluid mt-5">
    <?= $this->Form->create($resenha) ?>
    <fieldset>
        <legend class="text-center mb-5">
            <h3> ADICIONAR NOVA RESENHA </h3>
        </legend>
        <div class="card">
            <div class="card-body">
                <div class="form-row">
                    <div class="form-group col-md-12">
                        <strong><label>TITULO DA RESENHA</label></strong>
                        <?= $this->Form->control('titulo', ['class' => 'form-control', 'placeholder' => 'Título da Resenha', 'label' => false]) ?>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-12">
                        <strong><label>LIVRO</label></strong>
                        <?= $this->Form->control('livro_id', ['options' => $livros, 'empty' => 'Selecione o Livro', 'class' => 'form-control form-control-chosen', 'required'=>true, 'label' => false]) ?>
                    </div>
                </div>
                <div class="form-row">

                    <div class="col-md-4 ml-0 mt-2 row block">
                        <label class="">RESENHA ATIVA<A/label>
                        <div class="px-4 mb-">
                            <?= $this->Form->control('ativo', ['class' => 'ml-5 js-switch', 'type' => 'checkbox', 'label' => false]) ?>
                        </div>
                    </div>
                </div>

                <div class="form-row">

                    <div class="container justify-content-center mt-5">
                        <hr>
                        <h3 class="text-center mt-3"> RESENHA</h3>
                        <?= $this->Form->control('descricao', ['type'=>'textarea','class' => 'form-control', 'id' => 'editor-um', 'placeholder' => '', 'label' => false]) ?>
                    </div>
                </div>
            </div>
        </div>

        <div class=" justify-content-center text-center mb-5">
            <?= $this->Form->button('<i class="fas fa-save"></i> SALVAR', ['class' => 'btn btn-primary mt-5', 'escapeTitle' => false]) ?>
            <?= $this->Html->link('CANCELAR', ['controller' => 'Resenhas', 'action' => 'index'], ['class' => 'btn btn-danger mt-5', 'escape' => false]) ?>
            <?= $this->Form->end() ?>
        </div>
    </fieldset>
</div>

<?= $this->Html->script(['switchery','chosen.jquery.min','nprogress','jquery.maskedinput']) ?>
<?= $this->element('editor'); ?>

<script>
    $(".form-control-chosen").chosen();

   
</script>