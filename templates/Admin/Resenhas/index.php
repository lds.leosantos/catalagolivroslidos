<?php

/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Resenha[]|\Cake\Collection\CollectionInterface $resenhas
 */
?>


<?= $this->Html->css(['dataTables.bootstrap.min']) ?>
<?= $this->Html->script(['jquery.dataTables.min', 'dataTables.bootstrap4.min', 'dataTables.buttons.min', 'buttons.html5.min']) ?>

<div class="col-md-12">
    <div class="table-responsive mt-5">
        <h3>
            <?= $this->Html->link('<i class="fas fa-plus fa-2x"></i>', ['controller' => 'Resenhas', 'action' => 'add'], ['class' => 'btn btn-primary btn-lg rounded-circle float-right', 'escape' => false]) ?>
        </h3>
        <h3 class="text-center">
            RESENHAS
        </h3>
    </div>
    <br>
    <div class="card">

        <table id="datatable" class="table table-striped table-bordered table-hover text-center rounded">

            <thead>
                <tr>

                    <th>TÍTULO DA RESENHA</th>
                    <th>LIVRO</th>
                    <th>ATIVO</th>
                    <th>QUANTIDADE DE ACESSO</th>
                    <th>CURTIDAS</th>
                    <th>DATA CADASTRO</th>
                    <th class="text-center">AÇÕES</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($resenhas as $resenha) : ?>
                    <tr>

                        <td class="align-middle"><?= h($resenha->titulo) ?></td>
                        <td class="align-middle"><?= h($resenha->livro->nome_livro) ?></td>
                        <td class="align-middle"><?= h($resenha->ativo) ? __('SIM') : __('NÃO') ?></td>
                        <td class="align-middle"><?= $this->Number->format($resenha->qtn_acesso) ?></td>
                        <td class="align-middle"><?= $this->Number->format($resenha->curtidas) ?></td>
                        <td class="align-middle"><?= h($resenha->created) ?></td>
                        <td class="align-middle">
                            <?= $this->Html->link(('<i class="far fa-eye"></i>'), ['controller' => 'Resenhas', 'action' => 'view', $resenha->id], ['class' => 'btn btn-outline-primary', 'escape' => false]) ?>
                            <?= $this->Html->link('<i class="far fa-edit "></i>', ['controller' => 'Resenhas', 'action' =>  'edit', $resenha->id], ['class' =>  'btn btn-outline-warning',  'escape' => false]) ?>
                            <?= $this->Form->postLink('<i class="fas fa-trash"></i>', ['controller' => 'Resenhas', 'action' => 'delete', $resenha->id], ['class' => 'btn btn-outline-danger', 'escape' => false, 'confirm' => __('Realmente deseja apagar o Livro  # {0}?', $resenha->titulo)]) ?>
                        </td>

                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>