<?php

/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Resenha $resenha
 */
?>
<div class="container-fluid">
    <div class="col-md-12">
        <div class="table-responsive rounded mt-5">
            <h3>
                <?= $this->Html->link(
                    '<i class="fas fa-undo"></i> Retornar',
                    ['controller' => 'Resenhas', 'action' => 'index'],
                    ['class' => 'btn btn-info btn-lg float-right mb-5', 'escape' => false]
                ) ?>
            </h3>
        </div>

        <div class="container-fluid card mb-5 ">
            <div class="card-body">
                <h6 class="alert alert-success text-center">DADOS DA RESENHA</h6>
                <table class="table table-hover table-bordered rounded">
                    <tr>
                        <td>
                            <b>TITULO DA RESENHA: <?= h($resenha->titulo) ?></b>
                        </td>
                        <td>
                            <b>LIVRO: <?= h($resenha->livro->nome_livro) ?></b>
                        </td>
                        <td>
                            <b>QUANTIDADE DE ACESSO: <?= h($resenha->qtn_acesso) ?></b>
                        </td>
                        <td>
                            <b>CURTIDAS: <?= h($resenha->curtidas) ?></b>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <b>ATIVA: <?= h($resenha->ativo) ? __('SIM') : __('NÃO') ?></b>
                        </td>
                        <td>
                            <b> DATA DA CRIAÇÂO: <?= h($resenha->created) ?></b>
                        </td>
                        <td>
                            <b> DATA DA MODIFICAÇÂO: <?= h($resenha->modified) ?></b>

                        </td>
                        <td></td>
                    </tr>
                </table>
                <h6 class="alert alert-success text-center">RESENHA </h6>
                <table class="table table-hover table-bordered rounded">
                    <div class="container-fluid card mb-3">
                        <div class=" card-body mb-5"> <?php echo $resenha->descricao;   ?>
                        </div>
                    </div>
                </table>

            </div>
        </div>
    </div>
</div>