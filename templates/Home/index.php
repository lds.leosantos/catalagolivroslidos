<div class="row">
    <div class="col-lg-8 col-md-12">
        <div class="row">
            <?php foreach ($resenhasAtivas as $resenhasAtiva) { ?>
                <?php $dadoslivro = $this->Search->getlistarLivro($resenhasAtiva->livro_id);
                $comentarios = $this->Search->getlistarTotalComentarios($resenhasAtiva->id);
                ?>
                <div class="col-md-6 col-sm-12">
                    <div class="card h-100">
                        <div class="single-post post-style-1">
                            <div class="blog-image">
                                <?= $this->Html->image('../files/livros/' . $dadoslivro->id . '/' . $dadoslivro->imagem); ?>
                            </div>
                            <div class="blog-info">
                                <p> <?= $dadoslivro->nome_livro ?> </p>
                                <h4 class="title">
                                    <?= $this->Html->link(__($resenhasAtiva->titulo), ['controller' => 'Home', 'action' => 'view', $resenhasAtiva->id]); ?></b>
                                </h4>
                                <p class="lead">
                                    <?= $resenhasAtiva->descricao ?>
                                    <?= $this->Html->link(
                                        __('Contiunar Lendo'),
                                        ['controller' => 'Home', 'action' => 'view', $resenhasAtiva->id],
                                        ['class' => 'cont-lendo-post text-danger']
                                    ) ?>
                                </p>
                                </p>
                                <ul class="post-footer">
                                    <li>
                                        <?= $this->Html->link(('<i class="fas fa-heart"></i>' . $resenhasAtiva->curtidas), ['controller' => 'Home', 'action' => 'view', $resenhasAtiva->id], ['escape' => false]); ?>
                                    </li>
                                    <li>
                                        <?= $this->Html->link(('<i class="fas fa-comments"></i>' . $comentarios), ['controller' => 'Home', 'action' => 'view', $resenhasAtiva->id], ['escape' => false]); ?>
                                    </li>
                                    <li>
                                        <?= $this->Html->link(('<i class="fas fa-eye"></i>' . $resenhasAtiva->qtn_acesso), ['controller' => 'Home', 'action' => 'view', $resenhasAtiva->id], ['escape' => false]); ?>
                                    </li>
                                </ul>
                            </div><!-- blog-info -->
                        </div><!-- single-post -->
                    </div><!-- card -->
                </div><!-- col-md-6 col-sm-12 -->
            <?php } ?>
        </div><!-- row -->
    </div><!-- col-lg-8 col-md-12 -->

    <aside class="col-md-4">
        <div class="card shadow p-0 mb-3 bg-white">
            <div class="card-header">
                Resenhas em Destaques
            </div>
            <div class="p-3">
                <?php foreach ($resenhasDestaques as $resenhasDestaque) { ?>
                    <li class="media mb-2">
                        <div class="media-body anunc-title">
                            <?= $this->Html->link(__($resenhasDestaque->titulo), ['controller' => 'Home', 'action' => 'view', $resenhasDestaque->id]);?>
                        </div>
                    </li>
                <?php } ?>
            </div>
        </div>

        <div class="card shadow p-0 mb-3 bg-white">
            <div class="card-header">
                Últimas Resenhas Postadas
            </div>
            <div class="p-3">
                <?php foreach ($ultimasResenhas as $ultimasResenha) { ?>
                    <li class="media mb-2">
                        <div class="media-body anunc-title">
                            <?= $this->Html->link(__($ultimasResenha->titulo), ['controller' => 'Home', 'action' => 'view', $ultimasResenha->id]); ?></b>
                        </div>
                    </li>
                <?php } ?>
            </div>
        </div>
    </aside>
</div><!-- row -->