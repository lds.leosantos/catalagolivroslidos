<section class="blog-area section">
    <div class="container">
        <?php $dadoslivro = $this->Search->getlistarLivro($resenha->livro_id);
        ?>
        <div class="row">
            <div class="col-lg-2 col-md-0"></div>
            <div class="col-lg-8 col-md-12">
                <div class="post-wrapper">
                    <div class="blog-image">
                        <?= $this->Html->image('../files/livros/' . $dadoslivro->id . '/' . $dadoslivro->imagem, ['height' => '650']); ?>
                    </div>

                    <h3 class="title"><b><?= $resenha->titulo ?></b></h3>
                    <p class="para"> <?= $resenha->descricao ?>.</p>
                    <p><?= 'Data da Publicação: ' .  $resenha->created ?></p>
                    <?= $this->Form->create(null, ['url' => ['controller' => 'Home', 'action' => 'curtidas', $resenha->id]]) ?>
                    <ul class="post-footer">
                        <li>
                            <?= $this->Form->button(('<i class="fas fa-heart"></i> ' . $resenha->curtidas), ['class' => 'btn btn-danger mt-5', 'escapeTitle' => false]); ?>
                        </li>
                    </ul>
                    <?= $this->Form->end() ?>

                </div><!-- post-wrapper -->
            </div><!-- col-sm-8 col-sm-offset-2 -->

        </div><!-- row -->

        
    </div><!-- container -->
   
    <?php $mensagens = $this->Search->getlistarComentarios($resenha->id);

?>

<ul class="list-unstyled">

    <?php foreach ($mensagens as $mensagen) { ?>
        <hr>

        <li class="media">
            <div class="media-body">
                <h5 class="mt-0 mb-1">Nome: <?= $mensagen->nome_autor ?></h5>
                <p>Mensagem: <?= $mensagen->mensagem ?></p>
                <p>Data da Publicação: <?= $mensagen->created ?></p>
            </div>
        </li>

    <?php }; ?>
</ul>
<?= $this->Flash->render(); ?>
<?= $this->Form->create(null, ['url' => ['controller' => 'Home', 'action' => 'comentarios', $resenha->id]]) ?>
<div class="form-row">
    <div class="container justify-content-center mt-5">
        <hr>
        <h3 class="text-center mt-3">Comentário</h3>
        <?= $this->Form->control('nome_autor', ['class' => 'form-control', 'placeholder' => 'Digite seu nome', 'required' => true, 'label' => false]) ?>

        <?= $this->Form->control('mensagem', ['type' => 'textarea', 'class' => 'form-control', 'placeholder' => 'Digite sua Mensagem', 'required' => true, 'label' => false]) ?>
        <?= $this->Form->button('<i class="far fa-comment"></i> COMENTAR', ['class' => 'btn btn-primary mt-5', 'escapeTitle' => false]) ?>
    </div>
</div>
<div class="col-md-2 mt-3">
    <?php echo $this->Form->hidden('resenha_id');
    echo $this->Form->hidden('resenha_id', ['value' => $resenha->id]);
    ?>
</div>
<?= $this->Form->end() ?>
</section><!-- section -->