<?php


use Cake\Routing\Route\DashedRoute;
use Cake\Routing\RouteBuilder;

$routes->setRouteClass(DashedRoute::class);

// $routes->scope('/', function (RouteBuilder $builder) {
//     $builder->connect('/', ['prefix' => 'Admin', 'controller' => 'Users', 'action' => 'login']);
//     $builder->connect('/pages/*', ['controller' => 'Pages', 'action' => 'display']);
//     $builder->fallbacks();
// });

$routes->scope('/', function (RouteBuilder $builder) {
    $builder->connect('/',['controller' => 'Home', 'action' => 'index']);
    $builder->connect('/pages/*', ['controller' => 'Home', 'action' => 'index']);
    $builder->fallbacks();
});
$routes->prefix('admin', function (RouteBuilder $builder) {
    $builder->connect('/', ['prefix' => 'Admin', 'controller' => 'Users', 'action' => 'login']);
    $builder->fallbacks();
});
